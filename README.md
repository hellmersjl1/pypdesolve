# pypdesolve

This is a Python package used for the solution of partial differential equations.  The focus is on basic pedagogical solutions and creating interfaces to more advanced packges such as PETSc (https://www.mcs.anl.gov/petsc/).